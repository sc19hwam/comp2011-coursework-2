$(document).ready(function () {
    let csrf_token = $('meta[name=csrf-token]').attr('content');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    $(".post-likes").click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        let id;
        $(this).find('span').each(function () {
            const classList = $(this).attr('class').split(/\s+/);
            classList.forEach(function (e) {
                if (e.includes("post-likes")) {
                    const split = e.split('-');
                    id = split[split.length - 1];
                }
            })

        });
        toggleLike('/posts/' + id + '/toggle-like');
    })
});

let addCategoryUrl;
let userId;

function setUserId(id) {
    userId = id;
}

function initializeUrls(addCategory) {
    addCategoryUrl = addCategory;
}

function authorizationHeader(api_key) {
    $.ajaxSetup({
        headers: {
            "Authorization": "Basic " + btoa(api_key)
        },
    });
}

function addCategory(categoryName) {
    $.ajax({
        url: addCategoryUrl,
        method: "POST",
        data: {name: categoryName},
        dataType: 'json',
        success: function (response) {
            if (response.status === 'OK') {
                $("#edit-post #categories").append('<option value="' + response.category.id + '">' + response.category.name + '</option>');
                $('#add-category-modal').modal('hide');
            }

        },
        error: function (error) {
            console.error(error);
        }
    })
}


function addComment(commentUrl, comment) {
    if (comment && comment !== '') {
        $.ajax({
            url: commentUrl,
            method: "POST",
            data: {content: comment},
            dataType: 'json',
            success: function (response) {
                if (response.status === 'OK') {
                    $("#comments").prepend('<div class="card w-100 my-2"> ' +
                        '<div class="card-body"> ' +
                        '<p class="card-text"> ' +
                        +response.comment.content +
                        '</p> ' +
                        +'<div class="d-flex align-items-center justify-content-between">' +
                        '<p class="card-text text-muted"><small>' + response.comment.user.firstname + ' ' + response.comment.user.lastname + '</small></p> ' +
                        '<p class="card-text text-muted text-right"><small>Just now</small></p> ' +
                        '</div>' +
                        '</div> ' +
                        '</div>'
                    );
                }

            },
            error: function (error) {
                console.error(error);
            }
        });
    }

}

function toggleLike(likeUrl) {
    $.ajax({
        url: likeUrl,
        method: "POST",
        dataType: 'json',
        success: function (response) {
            if (response.status === 'OK') {
                $(".post-likes-" + response.postId).text(response.likes);
                    $(".post-likes-" + response.postId + " ~ i").toggleClass('fas');
                                        $(".post-likes-" + response.postId + " ~ i").toggleClass('far');
            }
        },
        error: function (error) {
            console.error(error);
        }
    })
}

function deletePost(deleteUrl){
    $.ajax({
        url: deleteUrl,
        method: 'DELETE',
        dataType: 'json',
        success: function (response) {
            if (response.status === 'OK') {
                $("#delete-post-modal").modal('hide');
                window.location.replace('/posts');
            }
        },
        error: function (error) {
            console.error(error);
        }
    })
}