from flask import render_template, flash, request, redirect, url_for, abort, jsonify
from app import app, db, models, login
from .forms import LoginForm, RegisterForm, PostForm
from flask_login import current_user, login_user, logout_user, login_required
import json
import datetime
import bcrypt
import base64
import string
import random


def check_authorization():
    api_key = request.args.get('api_key')
    if api_key:
        user = models.User.query.filter_by(api_key=api_key).first()
        if user:
            return user
    api_key = request.headers.get('Authorization')
    if api_key:
        api_key = api_key.replace('Basic ', '', 1)
        try:
            api_key = base64.b64decode(api_key).decode('utf-8')
        except TypeError:
            pass
        user = models.User.query.filter_by(api_key=api_key).first()
        if user:
            return user
    app.logger.error("Unauthorized")
    return None

@app.route('/', methods=["GET"])
def index():
    app.logger.info("Index Route")
    return render_template('index.html')


@app.route('/login', methods=["GET", "POST"])
def login():
    app.logger.error("Login route")
    if current_user.is_authenticated:
        app.logger.warning("User already authorized - user ID: {}".format(current_user.id))
        return redirect(url_for('index'))

    form = LoginForm()

    if request.method == "POST" and form.validate_on_submit():
        user = models.User.query.filter_by(email=form.email.data).first()
        if user is None or not bcrypt.checkpw(form.password.data.encode('utf-8'), user.password.encode('utf-8')):
            app.logger.info("Login Unsuccessful")
            flash('Invalid Email Address / Password combination')
        else:
            app.logger.info("Login Successful - user ID: {}".format(user.id))
            user.api_key = ''.join(
                random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=16))
            db.session.commit()
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('index'))

    return render_template('login.html', title='Login', form=form)


@app.route('/register', methods=["GET", "POST"])
def register():
    app.logger.info("Register route")
    if current_user.is_authenticated:
        app.logger.warning("User already authorized - user ID: {}".format(current_user.id))
        return redirect(url_for('index'))

    form = RegisterForm()

    if request.method == "POST" and form.validate_on_submit():
        user = models.User(email=form.email.data, firstname=form.firstname.data, lastname=form.lastname.data,
                           password=bcrypt.hashpw(form.password.data.encode("utf-8"), bcrypt.gensalt()).decode('utf-8'))
        db.session.add(user)
        db.session.commit()
        app.logger.info("Registration successful - user ID: {}".format(user.id))
        return redirect(url_for("login"))

    return render_template('register.html', title='Register', form=form)


@app.route('/logout')
def logout():
    app.logger.info('Logout route')
    if current_user.is_authenticated:
        logout_user()
    return redirect(url_for('index'))

@app.route('/categories', methods=["GET"])
def categories():
    app.logger.info('Categories route')
    categories = models.Category.query.all()
    return render_template('categories.html', title='Categories', categories=categories)

@app.route('/posts')
def posts():
    app.logger.info('Posts route')

    query = models.Post.query

    per_page = int(request.args.get('per_page')) if request.args.get('per_page') else 20
    page = int(request.args.get('page')) if request.args.get('page') else 1

    if request.args.get('author'):
        query = query.filter_by(author_id=request.args.get('author'))
    if request.args.get('category'):
        query = query.filter(models.Post.categories.any(id=request.args.get('category')))
    if request.args.get('after'):
        query = query.filter(models.Post.updated_at > datetime.datetime.fromisoformat(request.args.get('after')))
    if request.args.get('sort'):
        if request.args.get('sort') == "most_popular":
            query = query.order_by(models.Post.likes.count())
        elif request.args.get('sort') == "least_popular":
            query = query.order_by(models.Post.likes.count())
        else:
            query = query.order_by(models.Post.updated_at.desc())

    query = query.paginate(page, per_page)

    authors = models.User.query.all()
    categories = models.Category.query.all()

    return render_template("posts.html", title="All Posts", posts=query, authors=authors, categories=categories)


@app.route('/posts/<int:post_id>', methods=["DELETE"])
def delete_post(post_id):
    app.logger.info('Delete Post route')
    user = check_authorization()
    if not user:
        return None
    post = models.Post.query.get(post_id)
    if post.author_id != user.id:
        app.logger.error("Unauthorized user attempting to delete another user's post - user ID: {}, post ID: {}".format(user.id, post.id))
        return abort(401, "Unauthorized")
    db.session.delete(post)
    db.session.commit()
    return jsonify({"status": "OK"})


@app.route('/posts/<int:post_id>', methods=["GET"])
def post(post_id):
    app.logger.info('Get Post route - post ID: {}'.format(post_id))
    post = models.Post.query.get(post_id)
    if not post:
        app.logger.error("Post ID doesn't match any post on record - post ID: {}".format(post_id))
        abort(404, description="Post not found")

    post.content = post.content.replace('\n', '<br/>')

    return render_template("post.html", title=post.title, post=post)


@app.route('/posts/new', defaults={"post_id": -1}, methods=["GET", "POST"])
@app.route('/posts/<int:post_id>/edit', methods=["GET", "POST"])
@login_required
def edit_post(post_id):
    if post_id != -1:
        app.logger.info("Update Post route - post ID: {}".format(post_id))
    else:
        app.logger.info("Create Post route")

    post = models.Post.query.get(post_id)

    if post:
        if post.author_id != current_user.id:
            app.logger.error(
                "Unauthorized user attempting to update another user's post - user ID: {}, post ID: {}".format(current_user.id, post.id))
            return abort(401, "Unauthorized")
        form = PostForm(title=post.title, content=post.content, categories=[c.id for c in post.categories])
    else:
        form = PostForm()
    if request.method == "POST" and form.categories.data:
        form.categories.data = [int(c) for c in form.categories.data]

    form.categories.choices = [(c.id, c.name) for c in models.Category.query.order_by('name')]
    if request.method == 'POST' and form.validate_on_submit():
        categories = models.Category.query.filter(models.Category.id.in_([str(c) for c in form.categories.data])).all()
        if post:
            post.title = form.title.data
            post.content = form.content.data
            post.categories = categories
        else:
            post = models.Post(title=form.title.data, content=form.content.data, author_id=current_user.id,
                               categories=categories)
            db.session.add(post)

        db.session.commit()
        app.logger.info("Edit Post Successful - post ID: {}, user ID: {}".format(post.id, current_user.id))
        return redirect(url_for('post', post_id=post.id))

    return render_template("editpost.html", title='Edit Post', form=form)


@app.route('/posts/<int:post_id>/comment', methods=["POST"])
def new_comment(post_id):
    app.logger.info("Add Comment API route")
    user = check_authorization()
    if not user:
        return abort(401, description="Unauthorized")
    post = models.Post.query.get(post_id)

    if not post:
        app.logger.error("Post ID doesn't match any existing records - post ID: {}, user ID: {}".format(post_id, user.id))
        return abort(404, description="Post not found")


    comment = models.Comment(
        user_id=user.id,
        post_id=post_id,
        content=request.form['content']
    )

    db.session.add(comment)
    db.session.commit()

    app.logger.info("Comment added success - comment ID: {}".format(comment.id))

    return jsonify({'status': 'OK', 'comment': comment.to_dict()})


@app.route('/categories', methods=["POST"])
def add_category():
    app.logger.info("Add Category API route")
    user = check_authorization()
    if not user:
        return abort(401, description="Unauthorized")

    category = models.Category(name=request.form['name'])

    db.session.add(category)
    db.session.commit()

    app.logger.info("Comment successfully added - comment ID: {}".format(comment.id))

    return jsonify({'status': 'OK', 'category': category.to_dict()})


@app.route('/posts/<int:post_id>/toggle-like', methods=["POST"])
def toggle_like(post_id):
    app.logger.info("Toggle Like API Route")
    user = check_authorization()
    if not user:
        return abort(401, description="Unauthorized")
    post = models.Post.query.get(post_id)
    if not post:
        app.logger.error("Post ID doesn't match any existing records - post ID: {}, user ID: {}".format(post_id, user.id))
        return abort(404, description="Post not found")

    if post in user.likes:
        user.likes.remove(post)
    else:
        user.likes.append(post)

    db.session.commit()

    app.logger.info("Like successfully toggled - post ID: {}, user ID: {}".format(post.id, user.id))

    return jsonify({'status': 'OK', 'postId': post.id, 'likes': len(post.likes), 'userLiked': post in user.likes})


@app.context_processor
def inject_now():
    return {'now': datetime.datetime.now()}

@app.template_filter('datetime')
def format_datetime(value):
    # Template filter (date|datetime) to render date correctly
    format = "%a %d %b %Y %H:%M"
    return datetime.datetime.fromisoformat(str(value)).strftime(format)

@app.template_filter('find_user_by_id')
def find_user_by_id(id):
    return models.User.query.get(id)

@app.template_filter('find_category_by_id')
def find_category_by_id(id):
    return models.Category.query.get(id)
