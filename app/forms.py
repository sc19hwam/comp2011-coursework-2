from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, HiddenField, SelectMultipleField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError
from .models import User, Category


class LoginForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me')


class RegisterForm(FlaskForm):
    firstname = StringField('firstname', validators=[DataRequired()])
    lastname = StringField('lastname', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    password = StringField('password', validators=[DataRequired()])
    confirm_password = StringField('confirm_password',
                                   validators=[DataRequired(), EqualTo('password', message="Passwords need to match")])

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

        return True


class PostForm(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    content = TextAreaField('content', validators=[DataRequired()])
    categories = SelectMultipleField('categories', validators=[DataRequired()])