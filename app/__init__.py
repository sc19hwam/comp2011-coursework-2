from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_login import LoginManager
import logging
import datetime

logging.basicConfig(level=logging.DEBUG, filename="logs-{}.log".format(datetime.datetime.now().isoformat()))

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
login = LoginManager(app)
login.login_view = 'login'


migrate = Migrate(app, db)

from app import views, models

if __name__ == '__main__':
    app.run(debug=True)
