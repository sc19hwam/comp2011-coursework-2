from app import db, login
from flask_login import UserMixin
from sqlalchemy_serializer import SerializerMixin

postCategoryLinkEntity = db.Table('post_category_link_entity',
                                  db.Column('category_id', db.Integer(), db.ForeignKey('category.id'),
                                            primary_key=True),
                                  db.Column('post_id', db.Integer(), db.ForeignKey('post.id'), primary_key=True))

userPostLikeLinkEntity = db.Table('user_post_like_link_entity',
                                  db.Column('user_id', db.Integer(), db.ForeignKey('user.id'),
                                            primary_key=True),
                                  db.Column('post_id', db.Integer(), db.ForeignKey('post.id'), primary_key=True))


class User(db.Model, UserMixin, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(255))
    lastname = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    api_key = db.Column(db.String(255))
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    likes = db.relationship('Post', secondary=userPostLikeLinkEntity)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Post(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text())
    content = db.Column(db.Text())
    author_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime(), server_default=db.func.now())
    updated_at = db.Column(db.DateTime(), server_default=db.func.now(), server_onupdate=db.func.now())
    categories = db.relationship('Category', secondary=postCategoryLinkEntity)
    likes = db.relationship('User', secondary=userPostLikeLinkEntity)
    comments = db.relationship('Comment', backref='comment', lazy=True)


class Category(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    posts = db.relationship('Post', secondary=postCategoryLinkEntity)


class Comment(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer(), db.ForeignKey('post.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    content = db.Column(db.Text())
    created_at = db.Column(db.DateTime(), server_default=db.func.now())
    updated_at = db.Column(db.DateTime(), server_default=db.func.now(), server_onupdate=db.func.now())
    post = db.relationship('Post')
    user = db.relationship('User')
