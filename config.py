import os
from urllib.parse import quote

DB_URI_PROD = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="sc19hwam",
    password="qMqec7SrFdJ8",
    hostname="sc19hwam.mysql.pythonanywhere-services.com",
    databasename=quote("sc19hwam$coursework2"))

DB_URI_DEV = "postgresql://localhost:5432/cw2"

SQLALCHEMY_DATABASE_URI = DB_URI_DEV if os.environ.get('FLASK_ENV') == 'development' else DB_URI_PROD
SQLALCHEMY_TRACK_MODIFICATIONS = True
WTF_CSRF_ENABLED = True
SECRET_KEY='X$2ban#n10I68En6WCu?gyHmC'